export type CredentialForApp = {
    id?: string,
    url: string,
    login: string,
    password: string,
    createdAt: number,
	updatedAt: number,
}