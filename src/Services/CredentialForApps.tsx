import * as firebase from 'firebase';
import { PASSWORD_COLLECTION, USERS_COLLECTION } from '../Constants/Constants';
import { CredentialForApp } from '../types/types';

const credentialForApps = () => firebase
	.firestore()
	.collection(USERS_COLLECTION)
	.doc(firebase.auth().currentUser?.uid)
	.collection(PASSWORD_COLLECTION)

export const getCredentialForApps: () => Promise<CredentialForApp[]> = async () => {
	const querySnapshot = await credentialForApps().get()
	const docs = querySnapshot.docs

	return docs.map(doc => {
		const data = doc.data() as CredentialForApp
		return { ...data, id: doc.id }
	})
}

export const createCredentialForApps: (credLogin: string, credPassword: string, credUrl: string) => Promise<CredentialForApp> = async (credLogin, credPassword, credUrl ) => {
	const credentialForApp: CredentialForApp = {
		url: credUrl,
		login: credLogin,
		password: credPassword,
		createdAt: new Date().getTime(),
		updatedAt: new Date().getTime(),
	}
	const doc = await credentialForApps().add(credentialForApp)
	return { ...credentialForApp, id: doc.id }
}

export const updateCredentialForApps: (credentialForAppId: string, credLogin: string, credPassword: string, credUrl: string) => Promise<void> = async (credentialForAppId, credLogin, credPassword, credUrl) => {
	await credentialForApps().doc(credentialForAppId).update({
		url: credUrl,
		login: credLogin,
		password: credPassword,
		updatedAt: new Date().getTime(),
	})
}

export const deleteCredentialForApps: (credentialForAppId: string) => Promise<void> = async (credentialForAppId) => {
	await credentialForApps().doc(credentialForAppId).delete()
}