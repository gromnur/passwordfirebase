import * as firebase from 'firebase';

// Optionally import the services that you want to use
import "firebase/auth";
import "firebase/firestore";
//import "firebase/database";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDpdMVIdcutA9xLJlAYAQ65IH1l9M9yIHo",
    authDomain: "ynov-webmobile-fr.firebaseapp.com",
    projectId: "ynov-webmobile-fr",
    storageBucket: "ynov-webmobile-fr.appspot.com",
    messagingSenderId: "907321933809",
    appId: "1:907321933809:web:264e533c3e7c162cf38e50"
};

firebase.initializeApp(firebaseConfig);