import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthScreen from '../Screens/AuthScreen';
import HomeScreen from '../Screens/HomesScreen';
import NoteScreen from '../Screens/NoteScreen';
import { useAuth } from '../Contexts/AuthContext';
import { createStackNavigator } from '@react-navigation/stack';
import { CredentialForApp } from '../Types/types';

export type MainStackParamList = {
	AuthScreen: undefined
	HomeScreen: undefined
	NoteScreen: { credentialForApp: CredentialForApp }
}

const Stack = createStackNavigator<MainStackParamList>();

export default function MainRouter() {

	const auth = useAuth()

	const renderRoutes = () => {
		if (!auth.isSignedIn) 
			return <Stack.Screen name="AuthScreen" component={AuthScreen} />
		
		if (auth.isSignedIn)
			return <>
				<Stack.Screen name="HomeScreen" component={HomeScreen} />
				<Stack.Screen name="NoteScreen" component={NoteScreen} />
			</>
	}

	return (
		<NavigationContainer>
			<Stack.Navigator>
				{renderRoutes()}
			</Stack.Navigator>
		</NavigationContainer>
	);
}
